import './components/resizeable-panels-container';
import './components/resizeable-panel';
import './components/tabs-container';
import './components/tab-content';
